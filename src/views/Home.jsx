import React from 'react';
import mainImage from '../assets/logo.png';

import '../style.scss';

const Home = () => (
  <div className="neves__container">
    <div className="neves__avatar">
      <img
        src={mainImage}
        alt="Andre Neves"
      />
    </div>
    <div className="neves__name">
      André Neves
    </div>
    <div className="neves__role">
      Software Engineer
    </div>
    <div className="neves__links-container">
      <div className="neves__links-container-div">
        <a
          href="https://koalastud.io"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          Koala Studio
        </a>
        <a
          href="https://lightwork.app"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          Lightwork
        </a>
        <a
          href="https://lightningdecoder.com"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          Lightning Decoder
        </a>
      </div>
      <div className="neves__links-container-div">
        <a
          href="https://medium.com/@andreneves"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          Essays
        </a>
        <a
          href="https://github.com/andrerfneves"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          GitHub
        </a>
        <a
          href="https://twitter.com/andreneves"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          Twitter
        </a>
        <a
          href="https://www.linkedin.com/in/andrerfneves/"
          target="_blank"
          rel="noopener noreferrer"
          className="neves__link"
        >
          LinkedIn
        </a>
      </div>
    </div>
  </div>
);

export default Home;

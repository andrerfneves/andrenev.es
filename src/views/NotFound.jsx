import React from 'react';

import '../style.scss';

const NotFound = () => (
  <div className="Neves__container">
    <div className="Neves__not-found">
      <span>404</span>
      <p>Nothing to be found here</p>
    </div>
  </div>
);

export default NotFound;

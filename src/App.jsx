import React from 'react';
import {
  HashRouter,
  Route,
  Link,
  Switch,
} from 'react-router-dom';

import Home from './views/Home';
import NotFound from './views/NotFound';

const App = () => (
  <HashRouter>
    <main className="container">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route component={NotFound} />
      </Switch>
    </main>
  </HashRouter>
);

export default App;
